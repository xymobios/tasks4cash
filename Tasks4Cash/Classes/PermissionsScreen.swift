//
//  PermissionsScreen.swift
//  Tasks4Cash
//
//  Created by Nidhi Sharma on 8/3/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class PermissionsScreen: UIViewController, UITextFieldDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()

        updateViewComponents()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateViewComponents()
    {
        var screenHeight = UIScreen.mainScreen().bounds.size.height
        
        if screenHeight > 460 && screenHeight < 568 // 3.5 Inch device
        {
            
        }
        else if screenHeight >= 568 && screenHeight < 667 // 4 Inch device
        {
            
        }
        else if screenHeight >= 667 && screenHeight < 736 // iPhone6
        {

        }
        else //  iPhone6 Plus
        {
            
        }
    }
}
