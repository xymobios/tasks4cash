//
//  SignUpScreen.swift
//  Tasks4Cash
//
//  Created by Nidhi Sharma on 8/3/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class SignUpScreen: UIViewController, UITextFieldDelegate
{
    @IBOutlet var emailFieldTopConstraint : NSLayoutConstraint!
    @IBOutlet var scrollViewTopConstraint : NSLayoutConstraint!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var logoWidthConstraint : NSLayoutConstraint!
    @IBOutlet var logoHeightConstraint : NSLayoutConstraint!
    @IBOutlet var tasks4CashLabelTopConstraint : NSLayoutConstraint!
    @IBOutlet var signUpButtonBottomConstraint : NSLayoutConstraint!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        updateViewComponents()
        emailField.autocorrectionType = UITextAutocorrectionType.No
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func updateViewComponents()
    {
        var screenHeight = UIScreen.mainScreen().bounds.size.height
        
        if screenHeight > 460 && screenHeight < 568 // 3.5 Inch device
        {   
            logoWidthConstraint.constant = 90
            logoHeightConstraint.constant = 112
            tasks4CashLabelTopConstraint.constant = 6
            scrollViewTopConstraint.constant = 6
            emailFieldTopConstraint.constant = 25
            signUpButtonBottomConstraint.constant = 20
            setScrollViewContentSize()
        }
        else if screenHeight >= 568 && screenHeight < 667 // 4 Inch device
        {
            logoWidthConstraint.constant = 120
            logoHeightConstraint.constant = 150
            scrollViewTopConstraint.constant = 20
            emailFieldTopConstraint.constant = 30
            tasks4CashLabelTopConstraint.constant = 10
            signUpButtonBottomConstraint.constant = 20
        }
        else if screenHeight >= 667 && screenHeight < 736 // iPhone6
        {
            
            scrollViewTopConstraint.constant = 30
            emailFieldTopConstraint.constant = 50
        }
        else //  iPhone6 Plus
        {
            scrollViewTopConstraint.constant = 30
            emailFieldTopConstraint.constant = 50
            logoWidthConstraint.constant = 200
            logoHeightConstraint.constant = 250
        }
    }
    
    // Sign Up button Pressed
    @IBAction func SignUpPressed(sender: AnyObject)
    {
        NSLog("Email Address \(emailField.text)")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let permissionScreen = storyboard.instantiateViewControllerWithIdentifier("PermissionScreenStoryBoardID") as! PermissionsScreen
        
        self.navigationController?.pushViewController(permissionScreen, animated: true)
    }
    
    // MARK - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        
        var screenHeight = UIScreen.mainScreen().bounds.size.height
        
        if screenHeight > 460 && screenHeight < 568 // 3.5 Inch device
        {
            setScrollViewContentSize()
        }
        else
        {
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)
        }
        
        scrollView.contentOffset = CGPointMake(0.0, 0.0)
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        setScrollViewContentSize()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        setScrollViewContentOffset()
    }
    
    func setScrollViewContentSize()
    {
        var textFieldYOrigin = self.view.frame.size.height - scrollView.frame.origin.y + emailField.frame.origin.y + emailField.frame.size.height
        var visibleArea = self.view.frame.size.height - 216
        var visibleScrollViewArea = visibleArea - scrollView.frame.origin.y
        
        if textFieldYOrigin >= visibleArea
        {
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height + (scrollView.frame.size.height - visibleScrollViewArea))
        }
    }
    
    func setScrollViewContentOffset()
    {
        var visibleArea = self.view.frame.size.height - 216
        var visibleScrollViewArea = visibleArea - scrollView.frame.origin.y
        
        var yOffset =  visibleScrollViewArea - (10 + emailField.frame.size.height)

        scrollView.contentOffset = CGPointMake(0.0, emailField.frame.origin.y - 3)
    }
}
