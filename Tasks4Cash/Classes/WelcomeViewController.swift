//
//  WelcomeViewController.swift
//  Task4Cash
//
//  Created by Nidhi Sharma on 8/3/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController
{
    @IBOutlet var nextButtonBottomConstraint : NSLayoutConstraint!
    @IBOutlet var logoTopConstraint : NSLayoutConstraint!
    @IBOutlet var logoWidthConstraint : NSLayoutConstraint!
    @IBOutlet var logoHeightConstraint : NSLayoutConstraint!
    @IBOutlet var tasks4CashLabelTopConstraint : NSLayoutConstraint!
    @IBOutlet var textsContainerTopConstraint : NSLayoutConstraint!
    @IBOutlet var textsContainerBottomConstraint : NSLayoutConstraint!
    @IBOutlet var appNameLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        updateViewComponents()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func updateViewComponents()
    {
        var screenHeight = UIScreen.mainScreen().bounds.size.height
        
        if screenHeight > 460 && screenHeight < 568 // 3.5 Inch device
        {
            nextButtonBottomConstraint.constant = 20
            logoWidthConstraint.constant = 90
            logoHeightConstraint.constant = 112
            tasks4CashLabelTopConstraint.constant = 13
            textsContainerTopConstraint.constant = 13
            textsContainerBottomConstraint.constant = 10
        }
        else if screenHeight >= 568 && screenHeight < 667 // 4 Inch device
        {
            logoWidthConstraint.constant = 120
            logoHeightConstraint.constant = 150
            tasks4CashLabelTopConstraint.constant = 10
            textsContainerTopConstraint.constant = 10
            textsContainerBottomConstraint.constant = 14
        }
        else if screenHeight >= 667 && screenHeight < 736 // iPhone6
        {
            tasks4CashLabelTopConstraint.constant = 20
            textsContainerTopConstraint.constant = 30
        }
        else //  iPhone6 Plus
        {
            textsContainerTopConstraint.constant = 40
            nextButtonBottomConstraint.constant = 60
            logoWidthConstraint.constant = 200
            logoHeightConstraint.constant = 250
        }
    }
}
