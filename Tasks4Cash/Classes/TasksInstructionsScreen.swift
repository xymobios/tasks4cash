//
//  TasksInstructionsScreen.swift
//  Task4Cash
//
//  Created by Nidhi Sharma on 8/2/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class TasksInstructionsScreen: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
    
    @IBAction func sideMenuPressed(sender: AnyObject)
    {
        var width = (UIScreen.mainScreen().bounds.size.width - 74)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(74.0, self.view.bounds.size.height), false, 0.0)
        var success = self.view.drawViewHierarchyInRect(CGRectMake(-width, -0.0, self.view.bounds.size.width, self.view.bounds.size.height), afterScreenUpdates: true)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sideMenuScreen = storyboard.instantiateViewControllerWithIdentifier("SideMenuStoryboardID") as! SideMenuScreen
        
        sideMenuScreen.screenShotImage = image
        
        self.navigationController?.pushViewController(sideMenuScreen, animated: true)
    }
}

