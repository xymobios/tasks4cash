//
//  SideMenuScreen.swift
//  Task4Cash
//
//  Created by Nidhi Sharma on 7/31/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class SideMenuScreen: UIViewController
{
    
    @IBOutlet var capturedView: UIImageView!
    var screenShotImage = UIImage()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        self.capturedView.image = screenShotImage
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
    
    @IBAction func hideSideMenu(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}

