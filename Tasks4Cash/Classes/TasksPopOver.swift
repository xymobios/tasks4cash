//
//  TasksPopOver.swift
//  Task4Cash
//
//  Created by Nidhi Sharma on 7/31/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class TasksPopOver: UIViewController
{
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
    
    @IBAction func DeclinePressed(sender: AnyObject)
    {
        self.view.removeFromSuperview()
    }
}



