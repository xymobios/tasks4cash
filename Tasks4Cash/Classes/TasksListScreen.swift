//
//  TasksListScreen.swift
//  Task4Cash
//
//  Created by Nidhi Sharma on 7/31/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class TasksListScreen: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
        
    @IBAction func viewTasksPressed(sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("TasksPopOverStoryboard") as! UIViewController
        addChildViewController(controller)
        controller.view.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        self.view.addSubview(controller.view)
        controller.didMoveToParentViewController(self)
    }
    
    @IBAction func sideMenuPressed(sender: AnyObject)
    {
        var width = (UIScreen.mainScreen().bounds.size.width - 74)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(74.0, self.view.bounds.size.height), false, 0.0)
        var success = self.view.drawViewHierarchyInRect(CGRectMake(-width, -0.0, self.view.bounds.size.width, self.view.bounds.size.height), afterScreenUpdates: true)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}

